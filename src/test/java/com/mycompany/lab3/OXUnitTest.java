/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HP
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow1BY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"x", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow2BY_O_output_true() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow3BY_O_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinNoPlayBY_X_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow1BY_X_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow2BY_X_output_true() {
        String[][] table = {{"-", "-", "-"},{"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinRow3BY_X_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol1BY_X_output_true() {
        String[][] table = {{"X", "-", "-"}, {"X", "O", "-"}, {"X", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol2BY_X_output_true() {
        String[][] table = {{"-", "X", "O"}, {"-", "X", "-"}, {"O", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol3BY_X_output_true() {
        String[][] table = {{"O", "-", "X"}, {"-", "O", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol1BY_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"O", "X", "X"}, {"O", "-", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol2BY_O_output_true() {
        String[][] table = {{"X", "O", "X"}, {"X", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol3BY_O_output_true() {
        String[][] table = {{"X", "X", "O"}, {"X", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinX1BY_O_output_true() {
        String[][] table = {{"O", "X", "-"}, {"X", "O", "X"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinX1BY_X_output_true() {
        String[][] table = {{"X", "O", "-"}, {"-", "X", "O"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinX2BY_X_output_true() {
        String[][] table = {{"-", "O", "X"}, {"O", "X", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinX2BY_O_output_true() {
        String[][] table = {{"-", "X", "O"}, {"X", "O", "X"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDraw_output_true() {
        String[][] table = {{"O", "X", "X"}, {"X", "O", "O"}, {"X", "O", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OX.checkWin(table, currentPlayer));
    }
}
